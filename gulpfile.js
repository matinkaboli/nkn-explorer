/* eslint-disable */

const gulp = require('gulp');
const del = require('del');
const lint = require('gulp-eslint');
const replace = require('gulp-replace');
const webpack = require('webpack');
const config = require('./src/config.js');

gulp.task('clean', cb =>
  del(
    'build/**',
    cb
  )
);

gulp.task('copy', ['clean'], () =>
  [
    gulp.src(
      'src/static/**/*'
    )
    .pipe(gulp.dest('build/static')),
    gulp.src(
      'server/index.js'
    )
    .pipe(gulp.dest('build'))
  ]
);

gulp.task('dev:build', ['clean'], () => {
  webpack(require('./webpack/dev.js'), (err, stats) => {
    if (err) {
      throw new Error('webpack build failed', err);
    }

    console.log(stats.toString({
      assets: true,
      colors: true
    }));
  });
});

gulp.task('prod:build', ['clean'], cb =>
  webpack(require('./webpack/prod.js'), (err, stats) => {
    if (err) {
      throw new Error('webpack build failed', err);
    }

    console.log(stats.toString({
      assets: true,
      colors: true
    }));
    cb();
  })
);

gulp.task('dev:replace', ['clean'], cb => {
  gulp.src('server/load.css')
    .pipe(gulp.dest('build/static'));

  gulp.src('server/index.html')
    .pipe(replace('@@CDN@@', config.cdn))
    .pipe(gulp.dest('build'));

  cb();
});

gulp.task('prod:replace', ['clean'], cb => {
  gulp.src('server/load.css')
    .pipe(gulp.dest('build/static'));

  gulp.src('server/index.html')
    .pipe(replace('@@CDN@@', config.cdn))
    .pipe(gulp.dest('build'));

  cb();
});

gulp.task('default', ['clean', 'copy', 'dev:replace', 'dev:build']);
gulp.task('dev', ['clean', 'copy', 'dev:replace', 'dev:build']);
gulp.task('prod', ['clean', 'copy', 'prod:replace', 'prod:build']);
