export const homePage = '/';
export const blocks = '/blocks';
export const addresses = '/addresses';
export const globalNode = '/global-node';
export const blockDetail = '/:block';
export const transactions = '/transactions';
export const sigchain = '/sigchain';
export const transactionDetail = '/:transaction';
export const overView = '/:address';
export const notFound = '/404/:entity';

export const allRouts = [
  blocks,
  addresses,
  globalNode,
  blockDetail,
  transactions,
  transactionDetail,
  notFound,
];
