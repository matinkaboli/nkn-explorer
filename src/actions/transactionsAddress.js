import fetch from 'node-fetch';

import pageDetector from 'Root/helpers/pageDetector';

export default async (p, address) => {
  const data = await fetch(`https://api.nknx.org/addresses/${address}/transactions?page=${pageDetector(p)}`).then(res => res.json());

  return data;
};
