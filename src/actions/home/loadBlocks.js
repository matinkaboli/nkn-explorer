import fetch from 'node-fetch';

import store from 'Root/store';
import types from 'Root/actions';

export default async () => new Promise(async (resolve) => {
  const { blocks } = await fetch('https://api.nknx.org/blocks').then(res => res.json());

  store.dispatch({
    data: blocks.data,
    type: types.blocks.LOAD,
  });

  resolve();
});
