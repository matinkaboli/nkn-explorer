import store from 'Root/store';
import types from 'Root/actions';

import loadCoin from 'Root/helpers/coin';

export default async () => new Promise(async (resolve) => {
  const coinData = await loadCoin();

  store.dispatch({
    data: coinData,
    type: types.coin.LOAD,
  });

  resolve();
});
