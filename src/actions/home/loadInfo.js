import store from 'Root/store';
import types from 'Root/actions';

import loadInfo from 'Root/helpers/info';

export default async () => new Promise(async (resolve) => {
  const data = await loadInfo();

  store.dispatch({
    data,
    type: types.info.LOAD,
  });

  resolve();
});
