import info from './loadInfo';
import coin from './loadCoin';
import blocks from './loadBlocks';
import transactions from './loadTransactions';

export default async () => new Promise(async (resolve, reject) => {
  try {
    await Promise.all([
      info(),
      coin(),
      blocks(),
      transactions(),
    ]);

    resolve();
  } catch (error) {
    reject();
  }
});
