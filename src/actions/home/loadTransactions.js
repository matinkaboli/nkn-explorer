import fetch from 'node-fetch';

import store from 'Root/store';
import types from 'Root/actions';

export default async () => new Promise(async (resolve) => {
  const { transactions } = await fetch('https://api.nknx.org/transactions').then(res => res.json());

  store.dispatch({
    data: transactions.data,
    type: types.transactions.LOAD,
  });

  resolve();
});
