import loadHome from './loadHome';

export default () => {
  setInterval(() => {
    loadHome();
  }, 15000);
};
