export default {
  token: {
    LOGIN: 'token/LOGIN',
    LOGOUT: 'token/LOGOUT',
  },
  coin: {
    LOAD: 'coin/LOAD',
  },
  info: {
    LOAD: 'info/LOAD',
  },
  blocks: {
    LOAD: 'blocks/LOAD',
  },
  transactions: {
    LOAD: 'transactions/LOAD',
  },
};
