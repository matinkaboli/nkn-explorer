import fetch from 'node-fetch';

import pageDetector from 'Root/helpers/pageDetector';

export default async (p) => {
  const data = await fetch(`https://api.nknx.org/addresses?page=${pageDetector(p)}`).then(res => res.json());

  return data;
};
