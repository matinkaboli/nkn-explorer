import fetch from 'node-fetch';

import pageDetector from 'Root/helpers/pageDetector';

export default async (p, block) => {
  const b = Number.parseInt(block, 10);

  const data = await fetch(`https://api.nknx.org/blocks/${b + 1}/transactions2?page=${pageDetector(p)}`).then(res => res.json());

  return data;
};
