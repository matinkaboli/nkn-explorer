import fetch from 'node-fetch';

import loadTransactions from './loadTransactions';

export default async (transactions) => {
  const loadedTransactions = await loadTransactions(transactions);

  const t = loadedTransactions.map(
    async x => fetch(`https://api.nknx.org/transactions/${x.id}/payload`).then(res => res.json()),
  );

  const result = await Promise.all(t);

  return result;
};
