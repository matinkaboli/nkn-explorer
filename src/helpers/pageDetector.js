export default (page) => {
  if (!page) {
    return 1;
  }

  if (isNaN(page)) {
    return 1;
  }

  const p = Number.parseInt(page, 10);

  if (p === 0) {
    return 1;
  }

  return p;
};
