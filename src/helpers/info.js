import fetch from 'node-fetch';

export default async () => {
  const data = await fetch('https://api.nknx.org/network/stats').then(res => res.json());

  return data;
};
