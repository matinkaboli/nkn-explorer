import fetch from 'node-fetch';

export default async (transactions) => {
  const t = transactions.map(
    async x => fetch(`https://api.nknx.org/transactions/${x.hash}`).then(res => res.json()),
  );

  const result = await Promise.all(t);

  return result;
};
