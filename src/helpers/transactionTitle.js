import types from './transactionType';

export default (type) => {
  switch (type) {
    case types.transfer:
      return 'Transfer';
    case types.generate:
      return 'ID Generation';
    case types.coinbase:
      return 'Mining Reward';
    case types.sigchain:
      return 'Signature Chain';
    default:
      return 'Transfer';
  }
};
