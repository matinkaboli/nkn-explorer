import fetch from 'node-fetch';

export default async () => {
  const {
    data: {
      priceUsd,
      marketCapUsd,
      volumeUsd24Hr,
      changePercent24Hr,
    },
  } = await fetch('https://api.coincap.io/v2/assets/nkn').then(res => res.json());

  return {
    price: priceUsd,
    volume: volumeUsd24Hr,
    marketCap: marketCapUsd,
    changePercent: changePercent24Hr,
  };
};
