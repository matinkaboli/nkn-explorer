import queryString from 'query-string';

export default query => queryString.parse(query);
