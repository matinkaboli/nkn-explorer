export default (text, amount = 30) => `${text.slice(0, amount)}...`;
