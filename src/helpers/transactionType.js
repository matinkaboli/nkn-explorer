export default {
  coinbase: 'COINBASE_TYPE',
  generate: 'GENERATE_ID_TYPE',
  sigchain: 'SIG_CHAIN_TXN_TYPE',
  transfer: 'TRANSFER_ASSET_TYPE',
};
