export default (text) => {
  if (text.length > 40) {
    return 'hash';
  }

  const int = Number.parseInt(text, 10);

  if (!Number.isNaN(int)) {
    return 'block';
  }

  return 'address';
};
