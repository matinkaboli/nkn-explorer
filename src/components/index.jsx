import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import BlockDetail from 'Root/components/other/Block/BlockDetail';
import OverView from './pages/AddressTransaction';
import Home from './pages/Home';
import Transactions from './pages/AllTransactions';
import TransactionDetail from './other/Transaction/TransactionDetail';
import Blocks from './pages/Blocks';
import GlobalNode from './pages/GlobalNode';
import Addresses from './pages/Addresses';
import SigChain from './other/Transaction/SigChain';
import Layout from './other/Layout';
import {
  transactions, transactionDetail, homePage, overView,
  blocks, blockDetail, globalNode, addresses, sigchain,
  notFound,
} from '../constants/routes';
import NotFound from './pages/NotFound';

function App() {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route exact path={transactions} component={Transactions} />
          <Route
            exact
            path={transactions + transactionDetail}
            component={TransactionDetail}
          />
          <Route
            exact
            path={transactions + sigchain}
            component={SigChain}
          />
          <Route
            exact
            path={addresses + overView}
            component={OverView}
          />
          <Route exact path={blocks} component={Blocks} />
          <Route exact path={blocks + blockDetail} component={BlockDetail} />
          <Route exact path={globalNode} component={GlobalNode} />
          <Route exact path={addresses} component={Addresses} />
          <Route exact path={notFound} component={NotFound} />
          <Route exact path={homePage} component={Home} />
          <Redirect to={homePage} />
        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
