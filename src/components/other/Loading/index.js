import React from 'react';

import styles from './styles.less';

function Loading() {
  return (
    <div className={styles.container}>
      <svg className={styles.loadIcon}>
        <path fill="#070d59" d="M12,4V2A10,10 0 0,0 2,12H4A8,8 0 0,1 12,4Z" />
      </svg>
    </div>
  );
}

export default Loading;
