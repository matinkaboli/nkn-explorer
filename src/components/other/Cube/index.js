import React from 'react';
import classNames from 'classnames';

import types from 'Root/helpers/transactionType';
import fingerprintIcon from 'Root/images/fingerprints.svg';

import styles from './styles.less';

const cube = (type) => {
  switch (type) {
    case types.coinbase:
      return (
        <div className={classNames(styles.cube, styles.cube_purple,
          'p-vh-center')}
        >
          <span className="icon-mining c-vh-center" />
        </div>
      );
    case types.transfer:
      return (
        <div className={classNames(styles.cube, styles.cube_green,
          'p-vh-center')}
        >
          <span className="icon-both_directions c-vh-center" />
        </div>
      );
    case types.sigchain:
      return (
        <div className={classNames(styles.cube, styles.cube_red,
          'p-vh-center f-s-20')}
        >
          <span className="icon-link c-vh-center" />
        </div>
      );
    case types.generate:
      return (
        <div className={classNames(styles.cube, styles.cube_gray,
          'p-vh-center f-s-20')}
        >
          <img src={fingerprintIcon} alt="Fingerprint" width="22px" height="31px" />
        </div>
      );
    default:
      return null;
  }
};

export default cube;
