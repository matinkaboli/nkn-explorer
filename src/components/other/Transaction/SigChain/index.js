import React from 'react';
import classNames from 'classnames';

import styles from './styles.less';

const SigChain = (props) => {
  const relays = props.sigchain.sigchain_elems;

  relays.pop();
  relays.shift();

  return (
    <div className="row mb-4">
      <div className={classNames(styles.node_client_box, 'col-12 mb-1')}>
        <p className={styles.info}>
          <span className="icon-server pr-2" />
          {`Client ${props.sigchain.srcPubkey} sent data`}
        </p>
        {relays.map(x => (
          <p className={styles.info}>
            <span className="icon-down-arrow pr-2" />
            {`Node ${x.pubkey} relay data`}
          </p>
        ))}
        <p className={styles.info}>
          <span className="icon-server pr-2" />
          {`Client ${props.sigchain.destPubkey} received data`}
        </p>
      </div>
    </div>
  );
};

export default SigChain;
