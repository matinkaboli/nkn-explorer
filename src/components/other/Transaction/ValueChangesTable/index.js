import moment from 'moment';
import shortid from 'shortid';
import fetch from 'node-fetch';
import classNames from 'classnames';
import React, { Component, Fragment } from 'react';
import { Link, withRouter } from 'react-router-dom';

import loadPayload from 'Root/helpers/loadPayload';
import queryString from 'Root/helpers/queryString';
import Loading from 'Root/components/other/Loading';
import transactionType from 'Root/helpers/transactionType';
import transactionTitle from 'Root/helpers/transactionTitle';
import loadTransactionsAddressPageAction from 'Root/actions/transactionsAddress';
import { transactionDetail, transactions, sigchain } from 'Root/constants/routes';

import styles from './styles.less';

class valueChangesTable extends Component {
  state = {
    error: false,
    loaded: false,
    address: null,
    payload: null,
    transactions: [],
  }

  componentDidMount() {
    const { p } = queryString(this.props.location.search);

    loadTransactionsAddressPageAction(p, this.props.match.params.address)
      .then((data) => {
        loadPayload(data.data)
          .then((payloads) => {
            this.setState({
              loaded: true,
              payload: payloads,
              transactions: data.data,
            });
          });

      })
      .catch(() => {
        this.setState({
          error: true,
        });
      });

    fetch(`https://api.nknx.org/addresses/${this.props.match.params.address}`)
      .then(res => res.json())
      .then((data) => {
        this.setState({
          address: data,
        });
      });
  }

  render() {
    if (this.state.error) {
      return <p>Error</p>;
    }

    if (!this.state.loaded) {
      return <Loading />;
    }

    return (
      <Fragment>
        <div className={classNames(styles.simple_table, 'row')}>
          <div className="col-12">
            <div className={styles.scroll}>
              <table className="table">
                <thead>
                  <tr>
                    <th
                      scope="col"
                      width="50%"
                      className={styles.padding_left}
                    >
                      {'TxHash'}
                    </th>
                    <th scope="col" width="10%">Date</th>
                    <th scope="col" width="10%">Type</th>
                    <th scope="col" width="15%">Value</th>
                    <th scope="col" width="5%">
                      <div className="invisible">Value</div>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.transactions.map((cell, index) => {
                    const type = {
                      icon: 'icon-both_directions',
                    };

                    if (cell.txType === transactionType.coinbase) {
                      type.icon = 'icon-mining';
                    } else if (cell.txType === transactionType.sigchain) {
                      type.icon = 'icon-link';
                    } else if (cell.txType === transactionType.generate) {
                      type.icon = '';
                    }

                    return (
                      <tr key={shortid.generate()}>
                        <td className={styles.padding_left}>
                          <Link to={`${transactions}/${cell.hash}`}>
                            {cell.hash}
                          </Link>
                        </td>

                        <td style={{ color: 'black' }}>
                          {moment.utc(cell.created_at).fromNow()}
                        </td>

                        <td>
                          <span className={type.icon} />

                          {cell.txType === transactionType.generate
                            && <img
                                 width="13px"
                                 height="13px"
                                 alt="Fingerprint"
                                 src={fingerprintIcon}
                               />
                          }

                          &nbsp;

                          {transactionTitle(cell.txType)}
                        </td>

                        <td>
                          {this.state.payload[index].amount / 100000000}
                          &nbsp;
                          NKN
                        </td>

                        <td>
                          {
                            this.state.payload[index].senderWallet
                            === this.props.match.params.address
                            ? (
                              <span className={classNames(styles.green_arrow, 'icon-long-arrow-down')} />
                            )
                            : (
                              <span className={classNames(styles.red_arrow, 'icon-long-arrow-up')} />
                            )
                          }
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default withRouter(valueChangesTable);
