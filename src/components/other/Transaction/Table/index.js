import moment from 'moment';
import shortid from 'shortid';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import React, { Component, Fragment } from 'react';

import loadPayload from 'Root/helpers/loadPayload';
import queryString from 'Root/helpers/queryString';
import Loading from 'Root/components/other/Loading';
import fingerprintIcon from 'Root/images/fingerprints.png';
import transactionType from 'Root/helpers/transactionType';
import transactionTitle from 'Root/helpers/transactionTitle';
import loadTransactionsPageAction from 'Root/actions/transactions';
import loadTransactionsPageWithBlockAction from 'Root/actions/blockTransactions';
import { transactionDetail, transactions, sigchain } from 'Root/constants/routes';

import styles from './styles.less';

class TrsTable extends Component {
  state = {
    error: false,
    loaded: false,
    transactions: [],
  };

  componentDidMount() {
    const { p, block } = queryString(this.props.location.search);

    if (!block) {
      loadTransactionsPageAction(p)
      .then((data) => {
        this.setState({
          loaded: true,
          transactions: data.transactions.data,
        });
      })
      .catch(() => {
        this.setState({
          error: true,
        });
      });

      return;
    }

    loadTransactionsPageWithBlockAction(p, block)
      .then((data) => {
        this.setState({
          loaded: true,
          transactions: data.data,
        });
      })
      .catch(() => {
        this.setState({
          error: true,
        });
      });
  }

  render() {
    if (this.state.error) {
      return <p>Error</p>;
    }

    if (!this.state.loaded) {
      return <Loading />;
    }

    return (
      <Fragment>
        <div className={classNames(styles.simple_table, 'row')}>
          <div className="col-12">
            <div className={styles.scroll}>
              <table className="table">
                <thead>
                  <tr>
                    <th
                      scope="col"
                      width="50%"
                      className={styles.padding_left}
                    >
                      {'TxHash'}
                    </th>
                    <th scope="col" width="20%">Block</th>
                    <th scope="col" width="15%">Date</th>
                    <th scope="col" width="15%">Type</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.transactions.map(cell => {
                    const type = {
                      icon: 'icon-both_directions',
                    };

                    if (cell.txType === transactionType.coinbase) {
                      type.icon = 'icon-mining';
                    } else if (cell.txType === transactionType.sigchain) {
                      type.icon = 'icon-link';
                    } else if (cell.txType === transactionType.generate) {
                      type.icon = '';
                    }

                    return (
                      <tr key={shortid.generate()}>
                        <td className={styles.padding_left}>
                          <Link to={`/transactions/${cell.hash}`}>
                            {cell.hash}
                          </Link>
                        </td>
                        <td>
                          <Link to={`/blocks/${cell.block_height}`}>
                            {cell.block_height}
                          </Link>
                        </td>
                        <td>
                          {moment.utc(cell.created_at).fromNow()}
                        </td>
                        <td>
                          <span className={type.icon} />

                          {cell.txType === transactionType.generate
                            && <img
                                 width="13px"
                                 height="13px"
                                 alt="Fingerprint"
                                 src={fingerprintIcon}
                               />
                          }

                          &nbsp;

                          {transactionTitle(cell.txType)}
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default withRouter(TrsTable);
