import moment from 'moment';
import fetch from 'node-fetch';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import React, { Fragment, Component } from 'react';

import Copy from 'Root/components/other/Copy';
import Loading from 'Root/components/other/Loading';
import loadCoinAction from 'Root/actions/home/loadCoin';
import commaSeparator from 'Root/helpers/commaSeparator';
import transactionType from 'Root/helpers/transactionType';
import transactionTitle from 'Root/helpers/transactionTitle';

import Sigchain from '../SigChain';
import styles from './styles.less';

class Detail extends Component {
  state = {
    payload: null,
    confirmBlock: 0,
    transaction: null,
  };

  componentDidMount() {
    fetch(`https://api.nknx.org/transactions/${this.props.match.params.transaction}`)
      .then(res => res.json())
      .then((data) => {
        if (data.id) {
          fetch(`https://api.nknx.org/transactions/${data.id}/payload`)
            .then(res => res.json())
            .then((payloadData) => {
              fetch('https://api.nknx.org/blocks')
                .then(res => res.json())
                .then((blocks) => {
                  this.setState({
                    transaction: data,
                    payload: payloadData,
                    confirmBlock: blocks.blocks.data[0].header.height - data.block_id,
                });
              });
            });
        } else {
          this.setState({
            transaction: 'error',
          });
        }
      });

    loadCoinAction();
  }

  render() {
    const { coin } = this.props;
    const { transaction, payload } = this.state;

    if (!transaction) {
      return <Loading />;
    }

    if (transaction === 'error') {
      return <Redirect to={`/404/${this.props.match.params.transaction}`} />
    }

    const type = transactionTitle(transaction.txType);

    const toUSD = (Number.parseFloat(coin.price, 10) * payload.amount).toFixed(2);

    return (
      <Fragment>
        <div className="card">
          <div className="card-body">
            <div className="row">
              <div className="col-12">
                <h3 className={styles.title}>Transaction Details</h3>
              </div>
            </div>

            <hr className="my-0 hr" />

            <div className="row">
              <div className={classNames(styles.list, 'col-12')}>
                <ul className="list-group">
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>Transaction Hash:</h6>
                      </div>

                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={styles.list_text}>
                          {transaction.hash}

                          <Copy text={transaction.hash} />
                        </h6>
                      </div>
                    </div>
                  </li>

                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>Status:</h6>
                      </div>
                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <span className={classNames(styles.badge_success, 'badge badge-pill')}>
                          <span className="icon-checked pr-2" />
                          {'Success'}
                        </span>
                      </div>
                    </div>
                  </li>

                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>Type:</h6>
                      </div>

                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={styles.list_text}>
                          {type}
                        </h6>
                      </div>
                    </div>
                  </li>

                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>Block:</h6>
                      </div>

                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={styles.list_text}>
                          <a className="color-sky-blue">
                            <Link to={`/blocks/${transaction.block_height}`}>
                              {transaction.block_height}
                            </Link>

                            &nbsp;
                            &nbsp;

                            <span className={classNames(styles.default_badge, 'color-gray')}>
                              {this.state.confirmBlock}
                              &nbsp;
                              Block Confirmations
                            </span>
                          </a>
                        </h6>
                      </div>
                    </div>
                  </li>

                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>TimeStamp:</h6>
                      </div>

                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={styles.list_text}>
                          <span className="icon-clock pr-2" />
                          {moment.utc(transaction.created_at).fromNow()}

                          &nbsp;

                          ( {transaction.created_at} UTC )
                        </h6>
                      </div>
                    </div>
                  </li>

                  {payload.senderWallet &&
                    <li className="list-group-item">
                      <div className="row">
                        <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                          <h6 className={styles.list_label}>From:</h6>
                        </div>

                        <div className={classNames(styles.pl,
                          'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                        >
                          <h6 className={styles.list_text}>
                            <a className="color-sky-blue">
                              <Link to={`/addresses/${payload.senderWallet}`}>
                                {payload.senderWallet}
                              </Link>

                              <Copy text={payload.senderWallet} />
                            </a>
                          </h6>
                        </div>
                      </div>
                    </li>
                  }

                  {payload.recipientWallet &&
                    <li className="list-group-item">
                      <div className="row">
                        <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                          <h6 className={styles.list_label}>To:</h6>
                        </div>
                        <div className={classNames(styles.pl,
                          'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                        >
                          <h6 className={styles.list_text}>
                            <a className="color-sky-blue">
                              <Link to={`/addresses/${payload.recipientWallet}`}>
                                {payload.recipientWallet}
                              </Link>

                              <Copy text={payload.recipientWallet} />
                            </a>
                          </h6>
                        </div>
                      </div>
                    </li>
                  }

                  {payload.public_key &&
                    <li className="list-group-item">
                      <div className="row">
                        <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                          <h6 className={styles.list_label}>Public Key:</h6>
                        </div>
                        <div className={classNames(styles.pl,
                          'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                        >
                          <h6 className={styles.list_text}>
                            {payload.public_key}

                            <Copy text={payload.public_key} />
                          </h6>
                        </div>
                      </div>
                    </li>
                  }

                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>Value:</h6>
                      </div>

                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={styles.list_text}>
                          <span className="color-gray">
                            {`${commaSeparator(payload.amount || 0)} NKN`}
                          </span>

                          &nbsp;
                          &nbsp;

                          <span className={classNames(styles.default_badge, 'mr-2 color-indigo')}>
                            $
                            {commaSeparator(toUSD)}
                          </span>
                        </h6>
                      </div>
                    </div>
                  </li>
                  {transactionType.sigchain === transaction.txType
                    ? <li className="list-group-item" />
                    : ''
                  }
                </ul>

                {transactionType.sigchain === transaction.txType
                  ? <Sigchain sigchain={payload.sigchain} />
                  : null
                }
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}


export default connect(state => ({
  coin: state.coin,
}))(Detail);
