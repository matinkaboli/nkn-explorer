import classNames from 'classnames';
import React, { Fragment, Component } from 'react';

import commaSeparator from 'Root/helpers/commaSeparator';
import loadAddressesPageAction from 'Root/actions/addresses';

import styles from './styles.less';

class Banner extends Component {
  state = {
    sum: 0,
  };

  componentDidMount() {
    loadAddressesPageAction(1)
      .then((data) => {
        this.setState({
          sum: data.sumAddresses,
        });
      });
  }

  render() {
    return (
      <Fragment>
        <div className={classNames(styles.banner, 'p-v-center')}>
          <h1 className="c-v-center w-100">
            <span className={styles.number}>
              {commaSeparator(this.state.sum)}
            </span>

            <span className={classNames(styles.address, 'pl-2')}>Addreess</span>
          </h1>
        </div>
      </Fragment>
    );
  }
}

export default Banner;
