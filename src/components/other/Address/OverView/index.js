import fetch from 'node-fetch';
import classNames from 'classnames';
import { withRouter } from 'react-router-dom';
import React, { Fragment, Component } from 'react';

import Copy from 'Root/components/other/Copy';
import Loading from 'Root/components/other/Loading';
import commaSeparator from 'Root/helpers/commaSeparator';

import styles from './styles.less';

class OverView extends Component {
  state = {
    address: null,
  };

  componentDidMount() {
    fetch(`https://api.nknx.org/addresses/${this.props.match.params.address}`)
      .then(res => res.json())
      .then((data) => {
        this.setState({
          address: data,
        });
      });
  }

  render() {
    const { address } = this.state;

    if (!address) {
      return <Loading />;
    }

    return (
      <Fragment>
        <div className="card">
          <div className="card-body">
            <div className="row">
              <div className="col-12">
                <h3 className={styles.title}>Overview</h3>
              </div>
            </div>
            <hr className="my-0 hr" />
            <div className="row">
              <div className={classNames(styles.list, 'col-12')}>
                <ul className="list-group">
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>
                          {'Overview'}
                        </h6>
                      </div>
                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={classNames(styles.list_text)}>
                          {address.address}

                          <Copy text={address.address} />
                        </h6>
                      </div>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>Balance:</h6>
                      </div>
                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={classNames(styles.list_text)}>
                          {`${commaSeparator(Number.parseFloat(address.balance, 10).toFixed(2))} NKN`}
                        </h6>
                      </div>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>Number of transactions:</h6>
                      </div>
                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={classNames(styles.list_text)}>
                          {commaSeparator(address.count_transactions)}
                        </h6>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default withRouter(OverView);
