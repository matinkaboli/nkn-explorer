import moment from 'moment';
import shortid from 'shortid';
import classNames from 'classnames';
import { Link, withRouter } from 'react-router-dom';
import React, { Fragment, Component } from 'react';

import queryString from 'Root/helpers/queryString';
import Loading from 'Root/components/other/Loading';
import commaSeparator from 'Root/helpers/commaSeparator';
import loadAddressesPageAction from 'Root/actions/addresses';

import { overView, addresses } from '../../../../constants/routes';
import styles from './styles.less';

class AddressTable extends Component {
  state = {
    error: false,
    loaded: false,
    addresses: [],
  };

  componentDidMount() {
    const { p } = queryString(this.props.location.search);

    loadAddressesPageAction(p)
      .then((data) => {
        this.setState({
          loaded: true,
          addresses: data.addresses.data,
        });
      })
      .catch(() => {
        this.setState({
          error: true,
        });
      });
  }

  render() {
    if (this.state.error) {
      return <p>Error</p>;
    }

    if (!this.state.loaded) {
      return <Loading />;
    }

    return (
      <Fragment>
        <div className={classNames(styles.simple_table, 'row')}>
          <div className="col-12">
            <div className={styles.scroll}>
              <table className="table">
                <thead>
                  <tr>
                    <th scope="col" width="50%">Address</th>
                    <th scope="col" width="25%">Transactions</th>
                    <th scope="col" width="25%">Last Transaction</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.addresses.map(cell => (
                    <tr key={shortid.generate()}>
                      <td className={styles.padding_left}>
                        <Link to={`${addresses}/${cell.address}`}>
                          {cell.address}
                        </Link>
                      </td>
                      <td className="color-indigo">
                        {commaSeparator(cell.count_transactions)}
                      </td>
                      <td className="color-indigo">
                        {moment.utc(cell.last_transaction).fromNow()}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
};

export default withRouter(AddressTable);
