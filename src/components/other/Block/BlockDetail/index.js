import moment from 'moment';
import fetch from 'node-fetch';
import classNames from 'classnames';
import { Link, Redirect } from 'react-router-dom';
import React, { Fragment, Component } from 'react';

import config from 'Root/config';
import Copy from 'Root/components/other/Copy';
import Loading from 'Root/components/other/Loading';
import commaSeparator from 'Root/helpers/commaSeparator';

import styles from './styles.less';

class Detail extends Component {
  state = {
    block: null,
  };

  componentDidMount() {
    fetch(`https://api.nknx.org/blocks/${this.props.match.params.block}`)
      .then(res => res.json())
      .then((data) => {
        this.setState({
          block: data,
        });
      })
      .catch(() => {
        this.setState({
          block: {},
        });
      });
  }

  render() {
    const { block } = this.state;

    if (!block) {
      return <Loading />;
    }

    if (!block.id) {
      return <Redirect to={`/404/${this.props.match.params.block}`} />
    }

    return (
      <Fragment>
        <div className="card">
          <div className="card-body">
            <div className="row">
              <div className="col-12">
                <h3 className={styles.title}>
                  {'Blocks'}
                  <span className={classNames(styles.title_small, 'color-gray')}>
                    #
                    {block.header.height}
                  </span>
                </h3>
              </div>
            </div>
            <hr className="my-0 hr" />
            <div className="row">
              <div className={classNames(styles.list, 'col-12')}>
                <ul className="list-group">
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>Block Height:</h6>
                      </div>
                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={styles.list_text}>
                          #
                          {block.header.height}
                        </h6>
                      </div>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>TimeStamp:</h6>
                      </div>
                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={styles.list_text}>
                          <span className="icon-clock pr-2" />
                          {moment.utc(block.header.timestamp).fromNow()}

                          &nbsp;

                          ( {block.header.timestamp} UTC )
                        </h6>
                      </div>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>Transactions:</h6>
                      </div>
                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={styles.list_text}>
                          <span className={classNames(styles.default_badge)}>
                            <Link to={`/transactions?block=${block.header.height}&p=1`}>
                              {`${commaSeparator(block.transactions_count)} Transactions`}
                            </Link>
                          </span>
                        </h6>
                      </div>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>Mined by:</h6>
                      </div>
                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={styles.list_text}>
                          {block.header.benificiaryWallet}
                          <Copy text={block.header.benificiaryWallet} />
                        </h6>
                      </div>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>Hash:</h6>
                      </div>
                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={styles.list_text}>
                          {block.header.hash}
                          <Copy text={block.header.hash} />
                        </h6>
                      </div>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>Size:</h6>
                      </div>
                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={styles.list_text}>
                          {`${commaSeparator(block.size)} bytes`}
                        </h6>
                      </div>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <div className="row">
                      <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <h6 className={styles.list_label}>Transaction root:</h6>
                      </div>
                      <div className={classNames(styles.pl,
                        'col-xl-9 col-lg-9 col-md-12 col-sm-12 pt-xl-0 pt-lg-0 pt-md-2 pt-sm-2 pt-2')}
                      >
                        <h6 className={styles.list_text}>
                          {block.header.transactionsRoot}
                          <Copy text={block.header.transactionsRoot} />
                        </h6>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Detail;
