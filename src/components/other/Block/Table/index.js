import moment from 'moment';
import shortid from 'shortid';
import classNames from 'classnames';
import React, { Fragment, Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { blocks, blockDetail } from 'Root/constants/routes';

import queryString from 'Root/helpers/queryString';
import Loading from 'Root/components/other/Loading';
import loadBlocksPageAction from 'Root/actions/blocks';

import styles from './styles.less';

class BlockTable extends Component {
  state = {
    blocks: [],
    error: false,
    loaded: false,
  };

  componentDidMount() {
    const { p } = queryString(this.props.location.search);

    loadBlocksPageAction(p)
      .then((data) => {
        this.setState({
          loaded: true,
          blocks: data.blocks.data,
        });
      })
      .catch(() => {
        this.setState({
          error: true,
        });
      });
  }

  render() {
    if (this.state.error) {
      return <p>Error</p>;
    }

    if (!this.state.loaded) {
      return <Loading />;
    }

    return (
      <Fragment>
        <div className={classNames(styles.simple_table, 'row')}>
          <div className="col-12">
            <div className={styles.scroll}>
              <table className="table">
                <thead>
                  <tr>
                    <th scope="col" width="13%">Block</th>
                    <th scope="col" width="14%">Date</th>
                    <th scope="col" width="14%">Txn</th>
                    <th scope="col" width="13.2%">Miner</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.blocks.map(cell => (
                    <tr key={shortid.generate()}>
                      <td className={styles.padding_left}>
                        <Link to={`${blocks}/${cell.header.height}`}>
                          {cell.header.height}
                        </Link>
                      </td>
                      <td className="color-indigo">
                        {moment.utc(cell.header.created_at).fromNow()}
                      </td>
                      <td className="color-sky-blue">
                        <Link to={`/transactions?block=${cell.header.height}`}>
                          {cell.transactions_count}
                        </Link>
                      </td>
                      <td>
                        <a className="color-sky-blue">
                          <Link to={`/addresses/${cell.header.benificiaryWallet}`}>
                            {cell.header.benificiaryWallet}
                          </Link>
                        </a>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
};

export default withRouter(BlockTable);
