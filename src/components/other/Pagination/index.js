import classNames from 'classnames';
import { Link } from 'react-router-dom';
import React, { Component, Fragment } from 'react';

import styles from './styles.less';

const defaultProps = {
  initialPage: 1,
};

class Pagination extends Component {
  state = {
    pager: {},
  };

  componentWillMount() {
    if (this.props.items && this.props.items.length) {
      this.setPage(this.props.initialPage);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.items !== prevProps.items) {
      this.setPage(this.props.initialPage);
    }
  }

  setPage(page) {
    const { items } = this.props;
    let { pager } = this.state;

    if (page < 1 || page > pager.totalPages) {
      return;
    }

    pager = this.getPager(items.length, page);

    const pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);

    this.setState({
      pager,
    });

    this.props.onChangePage(pageOfItems);
  }

  getPager(totalItems, currentPage, pageSize) {
    // default to first page
    currentPage = currentPage || 1;

    // default page size is 10
    pageSize = pageSize || 10;

    // calculate total pages
    let totalPages = Math.ceil(totalItems / pageSize);

    let startPage, endPage;

    if (totalPages <= 10) {
      startPage = 1;
      endPage = totalPages;
    } else {
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
  }

  let startIndex = (currentPage - 1) * pageSize;
  let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

  // create an array of pages to ng-repeat in the pager control
  let pages = [...Array((endPage + 1) - startPage).keys()].map(i => startPage + i);

    // return object with all pager properties required by the view
    return {
        totalItems: totalItems,
        currentPage: currentPage,
        pageSize: pageSize,
        totalPages: totalPages,
        startPage: startPage,
        endPage: endPage,
        startIndex: startIndex,
        endIndex: endIndex,
        pages: pages
    };
  }

  render() {
    let { pager } = this.state;

    if (!pager.pages || pager.pages.length <= 1) {
      return null;
    }

    return (
      <ul className={classNames(styles.simple_pagination, 'pagination')}>
        <li className={classNames(this.props.page === 1 ? 'disabled' : '', 'page-item')}>
          <a
            className={classNames(styles.page_link_next_prev, 'page-link')}
          >
            <a href={`${this.props.link}${this.props.page - 1}`}>
              <span className="icon-expand-arrow-left" />
            </a>
          </a>
        </li>

        <li className="page-item">
          <div className="page-link paginate-text">
            Page
            &nbsp;
            {this.props.page}

            {this.props.sum
              && (
                <Fragment>
                  &nbsp;
                  of
                  &nbsp;
                  {this.props.sum}
                </Fragment>
              )
            }
          </div>
        </li>

        <li className={classNames(pager.currentPage === pager.totalPages ? 'disabled' : '', 'page-item')}>
          <button
            className={classNames(styles.page_link_next_prev, 'page-link')}
          >
              <a href={`${this.props.link}${this.props.page + 1}`}>
                <span className="icon-expand-arrow-right" />
              </a>
          </button>
        </li>
      </ul>
    );
  }
}

Pagination.defaultProps = defaultProps;

export default Pagination;
