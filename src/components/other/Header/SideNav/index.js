import Sidebar from 'react-sidebar';
import classNames from 'classnames';
import { Collapse } from 'reactstrap';
import { Link } from 'react-router-dom';
import React, { Component } from 'react';

import {
  blocks,
  addresses,
  globalNode,
  transactions,
} from 'Root/constants/routes';

import MainNet from '../../../shared/MainNet';
import styles from './styles.less';

class SideNave extends Component {
  state = {
    rotate: false,
    collapse: true,
    pullRight: true,
    sidebarOpen: false,
  };

  onSetSidebarOpen = (open) => {
    this.setState({
      sidebarOpen: open,
    });
  };


  toggle = () => {
    this.setState(state => ({
      collapse: !state.collapse,
      rotate: !state.rotate,
    }));
  };

  render() {
    return (
      <div className={classNames(styles.sidebar, 'w-100')}>
        <Sidebar
          sidebar={(
            <div>
              <div>
                <button
                  className={classNames(styles.close_btn, 'btn')}
                  onClick={() => this.onSetSidebarOpen(false)}
                >
                  <span className="icon-cancel-music" />
                </button>
              </div>
              <div className={styles.main_net}>
                <MainNet />
              </div>
              <ul className="list-group border-0">
                <li className="list-group-item border-0 pt-3">
                  <Link
                    to="/"
                  >
                    {'Home'}
                  </Link>
                </li>
                <li className={classNames(styles.toggle_list, 'list-group-item border-0 pt-3')}>
                  <div className="d-flex">
                    <div className="pr-2">
                      {'BlockChain'}
                    </div>
                    {/* <div className={classNames(this.state.rotate ?
                     'rotate-down' : '', 'rotate')}> */}
                    {/* <i className={classNames('icon-expand-arrow-right f-s-9')} /> */}
                    {/* </div> */}
                  </div>
                  <Collapse isOpen={this.state.collapse}>
                    <ul className="list-group border-0 pt-3">
                      <li className="list-group-item border-0 pt-3">
                        <Link
                          to={transactions}
                        >
                          {'Transactions'}
                        </Link>
                      </li>
                      <li className="list-group-item border-0 pt-3">
                        <Link
                          to={blocks}
                        >
                          {'Blocks'}
                        </Link>
                      </li>
                      <li className="list-group-item border-0 pt-3">
                        <Link
                          to={addresses}
                        >
                          {'Addresses'}
                        </Link>
                      </li>
                    </ul>
                  </Collapse>
                </li>
                <li className="list-group-item border-0 pt-3">
                  <Link
                    to={globalNode}
                  >
                    {'Global Nodes'}
                  </Link>
                </li>
                <li className="list-group-item border-0 pt-3">
                  <a
                    href="https://nknwallet.io"
                    target="_blank"
                  >
                    {'Wallet'}
                  </a>
                </li>
              </ul>
            </div>
          )}
          open={this.state.sidebarOpen}
          pullRight={this.state.pullRight}
          onSetOpen={this.onSetSidebarOpen}
          styles={{
            sidebar: {
              position: 'fixed',
              width: '314px',
              backgroundImage: 'linear-gradient(335deg, #19295b, #0d163a)',
            },
          }}
        >
          <button
            className={classNames(styles.toggle, 'toggle btn btn-link d-xl-none d-lg-block d-md-block d-sm-block d-block')}
            onClick={() => this.onSetSidebarOpen(true)}
          >
            <img
              alt="Menu"
              width="30px"
              height="30px"
              src={require('Root/images/menu.svg')}
            />
          </button>
        </Sidebar>
      </div>
    );
  }
}

export default SideNave;
