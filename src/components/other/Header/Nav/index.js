import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import { Nav, NavItem, NavLink } from 'reactstrap';

import {
  transactions, blocks, globalNode, addresses,
} from 'Root/constants/routes';
import styles from './styles.less';

const NavItems = () => (
  <Nav className={classNames(styles.nav, '')} navbar>
    <NavItem>
      <Link
        to="/"
        className={classNames(styles.padding_nav_item_right, 'nav-link')}
      >
        {'Home'}
      </Link>
    </NavItem>
    <NavItem className={styles.drop}>
      <div className={classNames('dropdown')}>
        <NavLink className={classNames(styles.padding_nav_item_right, 'nav-link dropbtn')}>
          {'Blockchain' }
          <span className="icon-caret-down pl-2 f-s-11" />
        </NavLink>
        <div className="dropdown-content">
          <Link to={transactions}>Transactions</Link>
          <Link to={blocks}>Block</Link>
          <Link to={addresses}>Addresses</Link>
        </div>
      </div>
    </NavItem>
    <NavItem>
      <Link to={globalNode} className={classNames(styles.padding_nav_item_right, 'nav-link')}>Global Nodes</Link>
    </NavItem>

    <NavItem>
      <a href="https://nknwallet.io" target="_blank" className={classNames('nav-link')}>Wallet</a>
    </NavItem>

  </Nav>
);

export default NavItems;
