import classNames from 'classnames';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { InputGroup, InputGroupAddon } from 'reactstrap';

import detectText from 'Root/helpers/detectText';

import styles from './styles.less';

class Search extends Component {
 state = {
   search: '',
 };

  handleChange = (event) => {
    this.setState({
      search: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const { search } = this.state;

    const type = detectText(search);

    if (type === 'hash') {
      global.location.pathname = `/transactions/${search}`;
    } else if (type === 'address') {
      global.location.pathname = `/addresses/${search}`;
    } else if (type === 'block') {
      global.location.pathname = `/blocks/${search}`;
    }

    this.setState({
      search: '',
    });
  };

  render() {
    return (
      <form
        className={classNames(styles.search)}
        onSubmit={this.handleSubmit}
      >
        <InputGroup>
          <input
            type="text"
            name="search"
            value={this.state.search}
            className="form-control"
            onChange={this.handleChange}
            placeholder="Search by Address/Txhash/Block"
          />

          <InputGroupAddon addonType="append">
            <button className="btn">
              <i className="icon-search" />
            </button>
          </InputGroupAddon>
        </InputGroup>
      </form>
    );
  }
}

export default withRouter(Search);
