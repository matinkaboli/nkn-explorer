import classNames from 'classnames';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { InputGroup, InputGroupAddon } from 'reactstrap';

import detectText from 'Root/helpers/detectText';

import styles from './styles.less';

class LgSearch extends Component {
    state = {
      search: '',
    };

  handleChange = (event) => {
    this.setState({
      search: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const { search } = this.state;

    const type = detectText(search);

    if (type === 'hash') {
      global.location.pathname = `/transactions/${search}`;
    } else if (type === 'address') {
      global.location.pathname = `/addresses/${search}`;
    } else if (type === 'block') {
      global.location.pathname = `/blocks/${search}`;
    }

    this.setState({
      search: '',
    });
  };

  render() {
    return (
      <form
        className={classNames(styles.search, 'mx-auto')}
        onSubmit={this.handleSubmit}
      >
        <div className={styles.lg_width}>
          <InputGroup>
            <input
              type="text"
              name="search"
              value={this.state.search}
              className={classNames(styles.input, 'form-control')}
              onChange={this.handleChange}
              placeholder="Search by Address/Txhash/Block"
            />

            <InputGroupAddon addonType="append">
              <button className="btn">
                {'Search'}
              </button>
            </InputGroupAddon>
          </InputGroup>
        </div>
        <div className={styles.sm_width}>
          <div className="row">
            <div className="col-12">
              <input
                type="text"
                name="search"
                value={this.state.search}
                className={classNames(styles.input, 'form-control')}
                onChange={this.handleChange}
                placeholder="Search by Address/Txhash/Block"
              />
            </div>
          </div>
          <div className="row">
            <div className="col-12 mt-4 text-center">
              <button className="btn mx-auto">
                {'Search'}
              </button>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

export default withRouter(LgSearch);
