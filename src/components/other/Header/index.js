import React, { Component } from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarBrand,
} from 'reactstrap';

import logo from 'Root/images/logo.png';
import { homePage } from 'Root/constants/routes';
import MainNet from 'Root/components/shared/MainNet';

import NavItems from './Nav';
import LgSearch from './LgSearch';
import Search from './Search';
import SideNav from './SideNav';
import styles from './styles.less';

class Header extends Component {
  state = {
    isOpen: false,
  };

  render() {
    let smallSearchBar = (
      <div className={styles.search_margin}>
        <Search />
      </div>
    );
    let navMargin = true;
    let lgSearchBar = null;

    if (global.location.pathname === homePage) {
      smallSearchBar = null;
      navMargin = false;

      lgSearchBar = (
        <div className={classNames(styles.lg_searchbar, 'row')}>
          <div className="col-12">
            <h1 className={styles.explore_title}>NKN Blockchain Explorer</h1>
            <div className="row justify-content-center">
              <div className={classNames(styles.lg_search_padding,
                'col-xl-7 col-lg-7 col-md-11 col-sm-12 col-12 mt-4 pt-xl-2 pt-lg-2 pt-md-2 pt-sm-0 pt-0')}
              >
                <LgSearch />
              </div>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 px-0">
            <Navbar className={styles.navbar} light expand="lg">
              <NavbarBrand>
                <Link to="/">
                  <img
                    src={logo}
                    alt="logo"
                    height="38.6px"
                    width="36.3pxpx"
                  />
                </Link>
              </NavbarBrand>
              <div className={classNames(styles.drop,
                'd-xl-block d-lg-block d-md-none d-sm-none d-none')}
              >
                <MainNet />
              </div>
              <div className={styles.search_box}>
                {smallSearchBar}
              </div>
              <Collapse isOpen={this.state.isOpen} navbar dir="rtl" className={styles.collapse}>
                <NavItems margin={navMargin} />
                {smallSearchBar}
              </Collapse>
            </Navbar>
          </div>
        </div>
        {lgSearchBar}
        <SideNav />
      </div>
    );
  }
}

export default Header;
