import React from 'react';
import classNames from 'classnames';

import twitterIcon from 'Root/images/twitter.svg';

import styles from './styles.less';

function Footer() {
  return (
    <div className={classNames(styles.footer, 'p-v-center')}>
      <footer className="c-v-center">
        <div className="container-fluid">
          <div className="row justify-content-center">
            <div className="col-11 px-lg-5 px-md-5 px-sm-4 pl-4 pr-3">
              <div
                className="row justify-content-between pl-2 pr-lg-2 pr-md-0 pr-sm-0 pr-0"
              >
                <div
                  className="col-lg-3 col-md-3 col-sm-3 col-3 pl-0 p-v-center"
                >
                  <a className={classNames(styles.api, 'c-v-center')} href="/">API</a>
                </div>
                <div className="col-lg-3 cl-md-9 col-sm-9 col-9">
                  <div className="row">
                    <div className="col-10 p-v-center">
                      <p className={
                        classNames(styles.copy_right, 'c-v-center w-100')}
                      >
                        {' 2019 - All right reserved (c)'}
                      </p>
                    </div>
                    <div className="col-2">
                      <a href="https://twitter.com/nkn_org">
                        <img
                          alt="twitter"
                          src={twitterIcon}
                        />
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default Footer;
