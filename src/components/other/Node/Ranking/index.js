import classNames from 'classnames';
import { connect } from 'react-redux';
import { Chart } from 'react-google-charts';
import React, { Fragment, Component } from 'react';

import loadInfoAction from 'Root/actions/home/loadInfo';

import styles from './styles.less';

class Ranking extends Component {
  componentDidMount() {
    loadInfoAction();
  }

  render() {
    const { info, network } = this.props;

    const data = [
      ['Country', 'Nodes'],
    ];

    for (const country of network) {
      data.push([country.country_code, country.count]);
    }

    return (
      <Fragment>
        <div className={classNames(styles.header, 'row justify-content-between')}>
          <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <h3 className={classNames(styles.title, 'mb-0')}>Node Distribution Ranking</h3>
          </div>
          <div className={classNames(styles.right_title_box,
            'col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12 '
              + 'mt-xl-0 mt-lg-0 mt-md-2 mt-sm-2 mt-2 pl-xl-0 pl-lg-0 pl-md-3 pl-sm-3 pl-3')}
          >
            <div className="row">
              <div className="col-6">
                <h6 className="mb-0">
                  <span className={styles.subject}>Full Node:</span>
                  <span className={styles.subject_num}>
                    &nbsp;
                    {info.node}
                  </span>
                </h6>
              </div>
              <div className="col-6 pl-0">
                <h6 className="mb-0">
                  <span className={styles.subject}>From (Countries):</span>
                  <span className={styles.subject_num}>
                    &nbsp;
                    {info.countries}
                  </span>
                </h6>
              </div>
            </div>
          </div>

        </div>
        <Chart
          width="100%"
          chartType="BarChart"
          loader={<div className={styles.loading}>Loading Chart...</div>}
          options={{
            height: 800,
            bar: {
              groupWidth: '95%',
            },
            title: 'Node Distribution Ranking',
            defaultColor: '#2c99ed',
          }}
          data={data}
        />
      </Fragment>
    );
  }
}

export default connect(state => ({
  info: state.info,
}))(Ranking);
