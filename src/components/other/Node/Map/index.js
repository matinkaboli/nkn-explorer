import React, { Fragment } from 'react';
import { Chart } from 'react-google-charts';
import classNames from 'classnames';
import styles from './styles.less';

const options = {
  backgroundColor: '#FFF',
  colorAxis: {
    minValue: 0,
    colors: ['#f4f9ff', '#2c99ed'],
  },
  defaultColor: '#ebedf1',
};

const Map = (props) => {
  const data = [
    ['Country', 'region'],
  ];

  for (const country of props.network) {
    data.push([country.country_code, country.count]);
  }

  return (
    <Fragment>
      <div className={classNames(styles.header, 'row')}>
        <div className="col-12">
          <h6>Full Node Distribution</h6>
        </div>
      </div>

      <hr className="hr my-0" />

      <Chart
        chartEvents={[
          {
            eventName: 'select',
            callback: ({ chartWrapper }) => {
              const chart = chartWrapper.getChart();
              const selection = chart.getSelection();
              if (selection.length === 0) return;
              const region = data[selection[0].row + 1];
            },
          },
        ]}
        data={data}
        width="100%"
        height="602px"
        options={options}
        chartType="GeoChart"
        loader={<div className={styles.loading}>Loading Chart...</div>}
      />
    </Fragment>
  );
};

export default Map;
