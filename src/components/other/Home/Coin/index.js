import classNames from 'classnames';
import { connect } from 'react-redux';
import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

import darkLogo from 'Root/images/dark-logo.png';
import commaSeparator from 'Root/helpers/commaSeparator';

import styles from './styles.less';

const Coin = (props) => {
  let price = 0;
  let volume = 0;
  let marketCap = 0;
  let isRising = false;
  let changePercent = '0';

  if (props.coin.changePercent) {
    isRising = props.coin.changePercent.includes('+');

    changePercent = Number.parseFloat(props.coin.changePercent, 10).toFixed(2);
  }

  if (props.coin.price) {
    price = Number.parseFloat(props.coin.price, 10).toFixed(3);
  }

  if (props.coin.marketCap) {
    marketCap = commaSeparator(Number.parseInt(props.coin.marketCap, 10));
  }

  if (props.coin.volume) {
    volume = commaSeparator(Number.parseInt(props.coin.volume, 10));
  }

  return (
    <Fragment>
      <div className="pl-4">
        <div className={styles.padding_top_part}>
          <div className="row">
            <div className="col-2 pl-1">
              <Link to="/">
                <img
                  alt="logo"
                  width="36.3px"
                  height="38.6px"
                  src={darkLogo}
                />
              </Link>
            </div>
            <div className="col-10 p-v-center">
              <h6 className={classNames(styles.title, 'c-v-center')}>
                {'NKN'}
              </h6>
            </div>
          </div>
          <div className="row mt-2">
            <div className="col-5 pl-1">
              <h6 className={styles.price}>
                {'$'}
                <b>{price}</b>
              </h6>
            </div>
            <div className="col-7">
              <h6 className={isRising ? styles.percentageGreen : styles.percentageRed}>
                <span className={isRising ? 'icon-long-arrow-up' : 'icon-long-arrow-down'} />
                {changePercent}
              </h6>
            </div>
          </div>
        </div>
        <hr className="my-0 hr" />
        <div className={styles.padding_top_part}>
          <div className="row">
            <div className="col-xl-4 col-lg-6 col-md-4 col-sm-4 col-4 pl-1 pr-0">
              <h6 className={styles.label}>Market cap:</h6>
            </div>
            <div className="col-xl-8 col-lg-6 col-md-8 col-sm-8 col-8 pl-0">
              <p className={styles.text}>
                $
                {marketCap}
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-xl-4 col-lg-6 col-md-4 col-sm-4 col-4 pl-1 pr-0">
              <h6 className={styles.label}>24h Vol:</h6>
            </div>
            <div className="col-xl-8 col-lg-6 col-md-8 col-sm-8 col-8 pl-0">
              <p className={styles.text}>
                $
                {volume}
              </p>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default connect(state => ({
  coin: state.coin,
}))(Coin);
