import { Line } from 'react-chartjs-2';
import React, { Fragment } from 'react';

import styles from './styles.less';

const data = {
  labels: [
    '2007',
    ' ',
    '2008',
    ' ',
    '2009',
    ' ',
    '2010',
    ' ',
    '2011',
    ' ',
    '2012',
    ' ',
    '2013',
    ' ',
    '2014',
    ' ',
    '2015',
    ' ',
    '2016',
  ],
  datasets: [
    {
      backgroundColor: 'transparent',
      borderColor: '#2c99ed',
      borderWidth: [2],
      lineTension: 0.0,
      data: [0, 2, 3, 2, 1, 4, 5, 2, 3, 2, 10, 6, 12, 8, 9, 8, 15, 11, 13],
      pointBorderColor: '#2c99ed',
      pointHoverBorderColor: '#2c99ed',
      pointBackgroundColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointBorderWidth: 2,
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHoverRadius: 4,
      pointHitRadius: 10,
    },
  ],
};

const options = {
  responsive: true,
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        ticks: {
          fontColor: '#86939e',
          fontSize: 12,
          stepSize: 1,
        },
        gridLines: {
          color: '#ebedf1',
          drawBorder: false,
        },
      }],
    yAxes: [
      {
        ticks: {
          display: false,
        },
        gridLines: {
          display: false,
          drawBorder: false,
        },
      }],
  },
};

const LineChart = () => (
  <Fragment>
    <div className="row">
      <div className="col-12">
        <h3 className={styles.title}>Latest Transactiond</h3>
      </div>
    </div>

    <hr className="hr my-0" />

    <div className="row justify-content-center">
      <div className="col-11 px-0 pt-3">
        <Line
          data={data}
          options={options}
        />
      </div>
    </div>
  </Fragment>
);
export default LineChart;
