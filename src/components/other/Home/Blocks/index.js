import moment from 'moment';
import shortid from 'shortid';
import classNames from 'classnames';
import { connect } from 'react-redux';

import React, { Fragment, Component } from 'react';
import { Link, withRouter } from 'react-router-dom';

import styles from './styles.less';

class BlocksList extends Component {
  handleClick = () => {
    this.props.history.push('/blocks');
  }

  render() {
    return (
      <Fragment>
        <div className="row">
          <div className="col-12">
            <h3 className={styles.title}>Latest Blocks</h3>
          </div>
        </div>

        <hr className="hr my-0" />

        <div className={classNames(styles.list, styles.scroll, 'pl-3')}>
          <ul className="list-group list-group-flush mb-3">
            {this.props.blocks.map(list => (
              <li className="list-group-item" key={shortid.generate()}>
                <div className="row">
                  <div className="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">
                    <div className={classNames(styles.cube, 'p-vh-center')}>
                      <span className="icon-sugar-cube c-vh-center" />
                    </div>
                  </div>
                  <div className={classNames(styles.second_box,
                    'col-xl-3 col-lg-3 col-md-3 col-sm-3 col-11 p-v-center')}
                  >
                    <div className="c-v-center">
                      <h6 className={classNames(styles.list_title,
                        'f-s-14')}
                      >
                        <Link to={`/blocks/${list.header.height}`}>
                          {list.header.height}
                        </Link>
                      </h6>
                      <p className={classNames(styles.list_text, 'f-s-8')}>
                        {moment.utc(list.header.created_at).fromNow()}
                      </p>
                    </div>
                  </div>
                  <div
                    className="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-12 px-0 p-v-center"
                  >
                    <div className={classNames(styles.position_relative,
                      'c-v-center')}
                    >
                      <h6 className={classNames(styles.list_title,
                        'f-s-12')}
                      >
                        <Link to={`/addresses/${list.header.benificiaryWallet || list.header.wallet}`}>
                          {list.header.benificiaryWallet || list.header.wallet}
                        </Link>
                      </h6>
                      <p className={classNames(styles.list_text,
                        'f-s-8')}
                      >
                        {'Miner'}
                      </p>
                    </div>
                  </div>
                  <div className={classNames(styles.padding_right_col,
                    'col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12')}
                  >
                    <h6 className="pt-1">
                      <span
                        className={classNames(styles.list_title, 'f-s-12')}
                      >
                        <Link to={`/transactions?block=${list.header.height}`}>
                          {`${list.transactions_count} Txns`}
                        </Link>
                      </span>
                    </h6>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </div>

        <div className={classNames(styles.list, 'pl-3')}>
          <hr className="hr my-0" />

          <div className="row justify-content-center">
            <div className="col-11 px-1">
              <button className={classNames(styles.button, 'btn')} onClick={this.handleClick}>
                <Link to="/blocks">
                  View All Blocks
                </Link>
              </button>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default withRouter(connect(state => ({
  blocks: state.blocks,
}))(BlocksList));
