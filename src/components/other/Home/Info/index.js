import classNames from 'classnames';
import { connect } from 'react-redux';
import React, { Fragment } from 'react';

import commaSeparator from 'Root/helpers/commaSeparator';

import styles from './styles.less';

const Info = (props) => {
  let nodes = 0;
  let blocks = 0;

  if (props.info.node) {
    const { node } = props.info;

    nodes = node;
  }

  if (props.info.block) {
    const { block } = props.info;

    blocks = block;
  }

  return (
    <Fragment>
      <div className="pl-4">
        <div className={classNames(styles.padding, 'row')}>
          <div className="col-3 pl-1 text-right">
            <p className={classNames(styles.icon, 'mb-0')}>
              <span className="icon-vector" />
            </p>
          </div>
          <div className="col-9 pl-1">
            <h6 className={styles.label}>Network nodes</h6>
            <p className={styles.text}>{commaSeparator(nodes)}</p>
          </div>
        </div>
        <hr className="my-0 hr" />
        <div className={classNames(styles.padding, 'row')}>
          <div className="col-3 pl-1 text-right">
            <p className={classNames(styles.icon, 'mb-0')}>
              <span className="icon-global" />
            </p>
          </div>
          <div className="col-9 pl-1">
            <h6 className={styles.label}>Latest Block</h6>
            <p className={styles.text}>{commaSeparator(blocks)}</p>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default connect(state => ({
  info: state.info,
}))(Info);
