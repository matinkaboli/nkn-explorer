import moment from 'moment';
import shortid from 'shortid';
import classNames from 'classnames';
import { connect } from 'react-redux';
import React, { Fragment, Component } from 'react';
import { Link, withRouter } from 'react-router-dom';

import shorten from 'Root/helpers/shorten';
import Cube from 'Root/components/other/Cube';
import transactionTitle from 'Root/helpers/transactionTitle';

import styles from './styles.less';

class TransactionList extends Component {
  handleClick = () => {
    this.props.history.push('/transactions');
  };

  render() {
    return (
      <Fragment>
        <div className="row">
          <div className="col-12">
            <h3 className={styles.title}>Latest Transactions</h3>
          </div>
        </div>

        <hr className="hr my-0" />

        <div className={classNames(styles.list, styles.scroll, 'pl-3')}>
          <ul className="list-group list-group-flush mb-3">
            {this.props.transactions.map((list) => {
              return (
                <li className="list-group-item" key={shortid.generate()}>
                  <div className="row">
                    <div className="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">
                      {Cube(list.txType)}
                    </div>
                    <div className={classNames(styles.second_box,
                      'col-xl-4 col-lg-4 col-md-4 col-sm-4 col-11 p-v-center')}
                    >
                      <div className="c-v-center">
                        <h6 className={classNames(styles.list_title,
                          'f-s-12')}
                        >
                          <Link to={`/transactions/${list.hash}`}>
                            {shorten(list.hash)}
                          </Link>
                        </h6>
                        <p className={classNames(styles.list_text, 'f-s-8')}>
                          {moment.utc(list.created_at).fromNow()}
                        </p>
                      </div>
                    </div>
                    <div
                      className="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 pr-4 p-v-center"
                    >
                      <div className={classNames(styles.position_relative,
                        'c-v-center')}
                      >
                        <h6 className={classNames(styles.list_title_second,
                          'f-s-14')}
                        >
                          {transactionTitle(list.txType)}
                        </h6>
                        <p className={classNames(styles.list_text,
                          'f-s-8')}
                        >
                          Type
                        </p>
                      </div>
                    </div>
                    <div className={classNames(
                      'col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12',
                    )}
                    >
                      <h6 className={classNames(styles.list_text_second,
                        'pt-1 mt-2 f-s-11')}
                      >
                        {`Block: ${list.block_height}`}
                      </h6>
                    </div>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>

        <div className={classNames(styles.list, 'pl-3')}>
          <hr className="hr my-0" />
          <div className="row justify-content-center">
            <div className="col-11 px-1">
              <button className={classNames(styles.button, 'btn')} onClick={this.handleClick}>
                <Link to="/transactions">
                  View All Transactions
                </Link>
              </button>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default withRouter(connect(state => ({
  transactions: state.transactions,
}))(TransactionList));
