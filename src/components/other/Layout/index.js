import classNames from 'classnames';
import React, { Fragment } from 'react';

import Header from '../Header';
import Footer from '../Footer';
import styles from './styles.less';

const Layout = props => (
  <Fragment>
    <Header />
    <div className={classNames('container-fluid h-100')}>
      <div className="row justify-content-center mt-5 py-5">
        <div className={classNames(styles.main,
          'col-lg-11 col-md-11 col-sm-12 col-12')}
        >
          <main className={styles.main_container}>{props.children}</main>
        </div>
      </div>
      <div className="row mt-5">
        <div className="col-12 px-0">
          <Footer />
        </div>
      </div>
    </div>
  </Fragment>
);


export default Layout;
