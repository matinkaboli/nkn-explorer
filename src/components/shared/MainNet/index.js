import classNames from 'classnames';
import React, { Fragment } from 'react';

import styles from './styles.less';

const MainNet = () => (
  <Fragment>
    <div className={styles.drop}>
      <div className="row justify-content-center">
        <div className="col-1 p-v-center">
          <div className={classNames(styles.drop_dot, 'c-v-center')} />
        </div>
        <div className="col-5 pl-0">Main net</div>
      </div>
    </div>
  </Fragment>
);

export default MainNet;
