import classNames from 'classnames';
import React, { Fragment } from 'react';
import { homePage } from 'Root/constants/routes';
import { Link, withRouter } from 'react-router-dom';

import searchImage from 'Root/images/search.png';

import styles from './styles.less';

const NotFound = props => (
  <Fragment>
    <div className="row justify-content-center">
      <div className="col-xl-5 col-lg-7 col-md-8 col-sm-9 col-10 text-center">
        <img
          src={searchImage}
          height="148px"
          width="148px"
          alt="search"
          className="mx-auto d-block mt-4"
        />
        <h1 className={classNames(styles.title, 'pt-4 mt-2')}>Search Not Found!!</h1>
        <p className={classNames(styles.text, 'px-5 pt-2')}>
          {'The search string you entered was:'}
          {' '}
          <span>{props.match.params.entity}</span>
          {' '}
          {'Sorry! This is an invalid search string'}
        </p>
        <Link to={homePage} className={classNames('btn mt-3 mb-5', styles.button)}>Back to home</Link>
        <br />
      </div>
    </div>
  </Fragment>
);

export default withRouter(NotFound);
