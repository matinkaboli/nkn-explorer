import fetch from 'node-fetch';
import React, { Fragment, Component } from 'react';

import Map from 'Root/components/other/Node/Map';
import Loading from 'Root/components/other/Loading';
import Ranking from 'Root/components/other/Node/Ranking';

class GlobalNode extends Component {
  state = {
    network: null,
  };

  componentDidMount() {
    fetch('https://api.nknx.org/network/countries')
      .then(res => res.json())
      .then((data) => {
        this.setState({
          network: data,
        });
      });
  }

  render() {
    const { network } = this.state;

    if (!network) {
      return <Loading />;
    }

    return (
      <Fragment>
        <div className="card">
          <div className="card-body">
            <Map network={network} />
          </div>
        </div>

        <div className="card  mt-5">
          <div className="card-body">
            <Ranking network={network} />
          </div>
        </div>
      </Fragment>
    );
  }
}

export default GlobalNode;
