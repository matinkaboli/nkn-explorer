import fetch from 'node-fetch';
import { withRouter } from 'react-router-dom';
import React, { Fragment, Component } from 'react';

import queryString from 'Root/helpers/queryString';
import Loading from 'Root/components/other/Loading';
import pageDetector from 'Root/helpers/pageDetector';
import loadBlocksPageAction from 'Root/actions/blocks';
import Pagination from 'Root/components/other/Pagination';
import BlockTable from 'Root/components/other/Block/Table';

let exampleItems = [...Array(100).keys()].map(i => ({
  id: (i + 1),
  name: `Item ${i + 1}`,
}));

class Blocks extends Component {
  state = {
    sum: 0,
    page: 1,
    blocks: null,
    OfItems: [],
    exampleItems: exampleItems,
  };

  componentDidMount() {
    const page = pageDetector(queryString(this.props.location.search).p);

    this.setState({
      page,
    });

    loadBlocksPageAction(1)
      .then((data) => {
        this.setState({
          loaded: true,
          sum: Number.parseInt(data.sumBlocks / 10 + 1, 10),
        });
      })
      .catch(() => {
        this.setState({
          error: true,
        });
      });
  }

  onChangePage = (pageOfItems) => {
    this.setState({
      pageOfItems: pageOfItems,
    });
  };

  render() {
    return (
      <Fragment>
        <div className="card">
          <div className="card-body">
            <div className="row px-3 py-2">
              <div
                className="col-lg-6 col-md-6 col-sm-6 col-6 p-v-center py-1"
              >
                <h1 className="page-title mb-0 c-v-center">Blocks</h1>
              </div>
              <div
                className="col-lg-6 col-md-6 col-sm-6 col-6 text-right py-1"
              >
                {/* {this.state.pageOfItems.map(item => */}
                {/* <div key={item.id}>{item.name}</div> */}
                {/* )} */}
                <div className="float-right">
                  <Pagination
                    link="/blocks?p="
                    sum={this.state.sum}
                    page={this.state.page}
                    items={this.state.exampleItems}
                    onChangePage={this.onChangePage}
                  />
                </div>
              </div>
            </div>

            <BlockTable />

            <div className="row px-3 py-2 mb-1">
              <div className="col-lg-12 col-md-12 col-sm-12 col-12 text-right py-1">
                <div className="float-right my-2">
                  <Pagination
                    link="/blocks?p="
                    sum={this.state.sum}
                    page={this.state.page}
                    items={this.state.exampleItems}
                    onChangePage={this.onChangePage}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default withRouter(Blocks);
