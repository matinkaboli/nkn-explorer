import React, { Component } from 'react';

import queryString from 'Root/helpers/queryString';
import pageDetector from 'Root/helpers/pageDetector';
import Pagination from 'Root/components/other/Pagination';
import TrsTable from 'Root/components/other/Transaction/Table';
import loadTransactionsPageAction from 'Root/actions/transactions';

import styles from './styles.less';

let exampleItems = [...Array(100).keys()].map(i => ({
  id: (i + 1),
  name: `Item ${i + 1}`,
}));

class Transactions extends Component {
  state = {
    sum: 0,
    page: 1,
    block: 0,
    noOf: false,
    pageOfItems: [],
    exampleItems: exampleItems,
  };

  componentDidMount() {
    const { p, block } = queryString(this.props.location.search);

    const page = pageDetector(p);

    this.setState({
      page,
    });

    if (!block) {
      loadTransactionsPageAction(1)
      .then((data) => {
        this.setState({
          sum: Number.parseInt(data.sumTransactions / 10 + 1, 10),
        });
      })

      return;
    }

    this.setState({
      block,
      noOf: true,
    });

    setTimeout(() => {
      fetch(`https://api.nknx.org/blocks/${this.state.block}`)
      .then(res => res.json())
      .then((blockData) => {
        this.setState({
          sum: Number.parseInt((blockData.transactions_count || 10) / 10, 10),
        });
      });
    }, 200);
  }

  onChangePage = (pageOfItems) => {
    this.setState({
      pageOfItems: pageOfItems,
    });
  };

  render() {
    return (
      <div className={styles.transaction}>
        <div className="card">
          <div className="card-body">
            <div className="row px-3 py-2">
              <div
                className="col-lg-6 col-md-6 col-sm-6 col-6 p-v-center py-1"
              >
                <h1 className="page-title mb-0 c-v-center">Transactions</h1>
              </div>
              <div
                className="col-lg-6 col-md-6 col-sm-6 col-6 text-right py-1"
              >
                <div className="float-right">
                  {this.state.noOf
                    ? (
                      <Pagination
                        sum={this.state.sum}
                        page={this.state.page}
                        items={this.state.exampleItems}
                        onChangePage={this.onChangePage}
                        link={`/transactions?block=${this.state.block}&p=`}
                      />
                    )
                    : (
                      <Pagination
                        sum={this.state.sum}
                        page={this.state.page}
                        items={this.state.exampleItems}
                        onChangePage={this.onChangePage}
                        link={`/transactions?p=`}
                      />
                    )
                  }

                </div>
              </div>
            </div>

            <TrsTable />

            <div className="row px-3 py-2 mb-1">
              <div className="col-lg-12 col-md-12 col-sm-12 col-12 text-right py-1">
                <div className="float-right my-2">
                  {this.state.noOf
                    ? (
                      <Pagination
                        sum={this.state.sum}
                        page={this.state.page}
                        items={this.state.exampleItems}
                        onChangePage={this.onChangePage}
                        link={`/transactions?block=${this.state.block}&p=`}
                      />
                    )
                    : (
                      <Pagination
                        sum={this.state.sum}
                        page={this.state.page}
                        items={this.state.exampleItems}
                        onChangePage={this.onChangePage}
                        link={`/transactions?p=`}
                      />
                    )
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Transactions;
