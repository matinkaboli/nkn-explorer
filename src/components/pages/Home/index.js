import React, { Fragment, Component } from 'react';

import loadHome from 'Root/actions/home/loadHome';
import Coin from 'Root/components/other/Home/Coin';
import Info from 'Root/components/other/Home/Info';
import Loading from 'Root/components/other/Loading';
import BlocksList from 'Root/components/other/Home/Blocks';
import LineChart from 'Root/components/other/Home/LineChart';
import refreshHomeInterval from 'Root/actions/home/refreshInterval';
import TransactionList from 'Root/components/other/Home/Transaction';

class Home extends Component {
  state = {
    loaded: false,
  };

  componentDidMount() {
    loadHome().then(() => {
      this.setState({
        loaded: true,
      });

      refreshHomeInterval();
    });
  }

  render() {
    const { loaded } = this.state;

    if (!loaded) {
      return <Loading />;
    }

    return (
      <Fragment>
        <div className="row mb-4">
          <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
            <div className="card">
              <div className="card-body">
                <Coin />
              </div>
            </div>
          </div>
          <div
            className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-4"
          >
            <div className="card">
              <div className="card-body">
                <Info />
              </div>
            </div>
          </div>
          {/* <div
            className="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 mt-xl-0 mt-lg-4 mt-md-4 mt-sm-4 mt-4"
          >
            <div className="card">
              <div className="card-body">
                <LineChart />
              </div>
            </div>
          </div> */}
        </div>

        <div className="row mt-4">
          <div className="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
            <div className="card">
              <div className="card-body">
                <BlocksList />
              </div>
            </div>
          </div>
          <div className="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 mt-xl-0 mt-lg-4 mt-md-4 mt-sm-4 mt-4">
            <div className="card">
              <div className="card-body">
                <TransactionList />
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Home;
