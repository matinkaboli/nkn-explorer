import fetch from 'node-fetch';
import React, { Component, Fragment } from 'react';

import queryString from 'Root/helpers/queryString';
import Loading from 'Root/components/other/Loading';
import pageDetector from 'Root/helpers/pageDetector';
import Pagination from 'Root/components/other/Pagination';
import OverView from 'Root/components/other/Address/OverView';
import ValueChangesTable from 'Root/components/other/Transaction/ValueChangesTable';

let exampleItems = [...Array(100).keys()].map(i => ({
  id: (i + 1),
  name: `Item ${i + 1}`,
}));

class AddressTransactions extends Component {
  state = {
    page: 1,
    address: null,
    pageOfItems: [],
    exampleItems: exampleItems,
  };

  onChangePage = (pageOfItems) => {
    this.setState({
      pageOfItems: pageOfItems,
    });
  };

  componentDidMount() {
    const page = pageDetector(queryString(this.props.location.search).p);

    this.setState({
      page,
    });

    fetch(`https://api.nknx.org/addresses/${this.props.match.params.address}`)
      .then(res => res.json())
      .then((data) => {
        this.setState({
          address: data,
        });
      });
  }

  render() {
    if (!this.state.address) {
      return <Loading />;
    }

    return (
         <Fragment>
           <div className="mb-3">
               <OverView />
           </div>
           <div className="card mt-4">
             <div className="card-body">
               <div className="row px-3 py-2">
                 <div
                     className="col-lg-6 col-md-6 col-sm-6 col-6 p-v-center py-1"
                 >
                   <h1 className="page-title mb-0 c-v-center">Transactions</h1>
                 </div>
                 <div
                     className="col-lg-6 col-md-6 col-sm-6 col-6 text-right py-1"
                 >
                   <div className="float-right">
                     <Pagination
                       page={this.state.page}
                       items={this.state.exampleItems}
                       onChangePage={this.onChangePage}
                       link={`/addresses/${this.props.match.params.address}?p=`}
                       sum={Number.parseInt(this.state.address.count_transactions / 10)}
                     />
                   </div>
                 </div>
               </div>

               <ValueChangesTable />

               <div className="row px-3 py-2 mb-1">
                 <div className="col-lg-12 col-md-12 col-sm-12 col-12 text-right py-1">
                   <div className="float-right my-2">
                     <Pagination
                       page={this.state.page}
                       items={this.state.exampleItems}
                       onChangePage={this.onChangePage}
                       link={`/addresses/${this.props.match.params.address}?p=`}
                       sum={Number.parseInt(this.state.address.count_transactions / 10)}
                     />
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </Fragment>
    );
  }
}

export default AddressTransactions;
