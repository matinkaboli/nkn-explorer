import React, { Fragment, Component } from 'react';

import queryString from 'Root/helpers/queryString';
import pageDetector from 'Root/helpers/pageDetector';
import AddressTable from '../../other/Address/Table';
import Pagination from 'Root/components/other/Pagination';
import Banner from 'Root/components/other/Address/Banner';
import loadAddressesPageAction from 'Root/actions/addresses';

let exampleItems = [...Array(100).keys()].map(i => ({
  id: (i + 1),
  name: `Item ${i + 1}`,
}));

class Blocks extends Component {
  state = {
    sum: 0,
    page: 1,
    pageOfItems: [],
    exampleItems: exampleItems,
  };

  componentDidMount() {
    const page = pageDetector(queryString(this.props.location.search).p);

    this.setState({
      page,
    });

    loadAddressesPageAction(1)
      .then((data) => {
        this.setState({
          loaded: true,
          sum: Number.parseInt(data.sumAddresses / 10 + 1, 10),
        });
      })
  }

  onChangePage = (pageOfItems) => {
    this.setState({
      pageOfItems: pageOfItems,
    });
  };

  render() {
    return (
        <Fragment>
          <Banner />

          <br/>
          <div className="card mt-2">
            <div className="card-body">
              <div className="row px-3 py-2">
                <div
                    className="col-lg-6 col-md-6 col-sm-6 col-6 p-v-center py-1"
                >
                  <h1 className="page-title mb-0 c-v-center">Address</h1>
                </div>
                <div
                    className="col-lg-6 col-md-6 col-sm-6 col-6 text-right py-1"
                >
                  <div className="float-right">
                    <Pagination
                      sum={this.state.sum}
                      link="/addresses?p="
                      page={this.state.page}
                      items={this.state.exampleItems}
                      onChangePage={this.onChangePage}
                    />
                  </div>
                </div>
              </div>

              <AddressTable/>

              <div className="row px-3 py-2 mb-1">
                <div className="col-lg-12 col-md-12 col-sm-12 col-12 text-right py-1">
                  <div className="float-right my-2">
                    <Pagination
                      sum={this.state.sum}
                      link="/addresses?p="
                      page={this.state.page}
                      items={this.state.exampleItems}
                      onChangePage={this.onChangePage}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Fragment>
    );
  }
}

export default Blocks;
