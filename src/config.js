const config = {
  port: 3030,
  cdn: '/static',
  rpcAddr: 'https://devnet-seed.nkn.org',
  coincap: 'https://api.coincap.io/v2/assets/nkn',
};

// if (process.env.NODE_ENV === 'development') {
//   config.cdn = '';
// } else {
//   config.cdn = '';
// }

module.exports = config;
