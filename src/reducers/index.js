import { combineReducers } from 'redux';

import info from './info';
import coin from './coin';
import blocks from './blocks';
import transactions from './transactions';

export default combineReducers({
  info,
  coin,
  blocks,
  transactions,
});
