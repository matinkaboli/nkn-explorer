import types from 'Root/actions';

export default (state = [], action) => {
  switch (action.type) {
    case types.info.LOAD: {
      return {
        node: action.data.totalNodes,
        block: action.data.totalBlocks,
        countries: action.data.totalCountries,
        providers: action.data.totalProviders,
      };
    }

    default: {
      return state;
    }
  }
};
